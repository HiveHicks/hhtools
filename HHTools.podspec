#
# Be sure to run `pod lib lint HHTools.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "HHTools"
  s.version          = "0.1.0"
  s.summary          = "Objective-C extensions and tools"
  s.description      = <<-DESC
                       My set of extensions and helpers for iOS development
                       DESC
  s.homepage         = "https://bitbucket.org/HiveHicks/hhtools"
  s.license          = 'MIT'
  s.author           = { "HiveHicks" => "hivehicks@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/HiveHicks/hhtools", :tag => s.version.to_s }

  s.platform     = :ios, '6.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/Public'
  s.resource_bundles = {
    'HHTools' => ['Pod/Assets/*.png']
  }

  s.public_header_files = 'Pod/Classes/Public/*.h'
  s.frameworks = 'UIKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
