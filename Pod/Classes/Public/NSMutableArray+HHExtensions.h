//
// Created by HiveHicks on 25.05.14.
//

#import <Foundation/Foundation.h>


@interface NSMutableArray (HHExtensions)

- (BOOL)addNonNilObject:(id)object;

/// Добавляет объект в массив, если его там нет, или заменяет новым существующий объект, эквивалентный ему (isEqual:)
- (void)addOrReplaceObject:(id)object;

@end