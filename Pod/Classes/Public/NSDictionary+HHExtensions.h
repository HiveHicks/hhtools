//
// Created by HiveHicks on 11.03.14.
//

#import <Foundation/Foundation.h>


@interface NSDictionary (HHExtensions)

- (id)firstKeyForValue:(id)value;

- (NSArray *)arrayByMappingKeysAndValuesUsingBlock:(id (^)(id key, id value))block;

- (NSString *)queryString;

- (NSDictionary *)dictionaryByAddingEntriesFromDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryByRemovingNSNullValues;

@end