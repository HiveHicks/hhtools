//
// Created by HiveHicks on 14.04.14.
//

#import "NSCalendar+HHExtensions.h"


@implementation NSCalendar (HHExtensions)

- (NSDate *)dateByAddingHours:(NSInteger)hours toDate:(NSDate *)date
{
    NSDateComponents *components = [NSDateComponents new];
    components.hour = hours;

    return [self dateByAddingComponents:components toDate:date options:0];
}

- (NSDate *)dateByAddingDays:(NSInteger)days toDate:(NSDate *)date
{
    NSDateComponents *components = [NSDateComponents new];
    components.day = days;

    return [self dateByAddingComponents:components toDate:date options:0];
}

- (NSDate *)dateByAddingMonths:(NSInteger)months toDate:(NSDate *)date
{
    NSDateComponents *components = [NSDateComponents new];
    components.month = months;

    return [self dateByAddingComponents:components toDate:date options:0];
}

- (NSDate *)dateByAddingYears:(NSInteger)years toDate:(NSDate *)date
{
    NSDateComponents *components = [NSDateComponents new];
    components.year = years;

    return [self dateByAddingComponents:components toDate:date options:0];
}

@end