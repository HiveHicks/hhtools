//
// Created by Andrey Yutkin on 21.02.14.
//

#import "NSNotificationCenter+HHExtensions.h"


@implementation NSNotificationCenter (HHExtensions)

- (void)postNotificationOnMainThreadWithName:(NSString *)name object:(id)object
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self postNotificationName:name object:object];
    });
}

@end