//
// Created by HiveHicks on 18.01.15.
//

#import <Foundation/Foundation.h>


@interface HHConcurrentOperation : NSOperation

- (void)setExecuting:(BOOL)executing;
- (void)setFinished:(BOOL)finished;

- (void)completeOperation;

@end