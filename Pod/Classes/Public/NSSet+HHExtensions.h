//
// Created by HiveHicks on 09.03.14.
//

#import <Foundation/Foundation.h>


@interface NSSet (HHExtensions)

- (NSSet *)setByRemovingObjects:(NSSet *)set;

@end