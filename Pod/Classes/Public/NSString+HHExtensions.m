//
// Created by Andrey Yutkin on 05.02.14.
//

#import "NSString+HHExtensions.h"


@implementation NSString (HHExtensions)

+ (BOOL)isNilOrEmpty:(NSString *)string
{
    return string == nil || [string isEmpty];
}

- (BOOL)isEmpty
{
    return [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0;
}

- (BOOL)isValidEmail
{
    if ([NSString isNilOrEmpty:self]) {
        return NO;
    }

    NSString *filterLikeOnAwadServer =
            @"^[-a-zA-Zа-яА-Я0-9!#$%&'*+/=?^_`{|}~]+(?:\\.[-a-zA-Zа-яА-Я0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-zA-Zа-яА-Я0-9]\\.?([-a-zA-Zа-яА-Я0-9]{0,61}[a-zA-Zа-яА-Я0-9])?)*\\.(?:[a-zA-Zа-яА-Я]{1,})$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", filterLikeOnAwadServer];
    return [emailTest evaluateWithObject:self];
}

- (NSString *)URLEncodedString
{
    NSString *escapedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
            NULL,
            (__bridge CFStringRef)self,
            NULL,
            CFSTR(":/?#[]!$ &'()*+,;=\"<>%{}|\\^~`"),
            kCFStringEncodingUTF8
    ));

    if (escapedString) {
        return escapedString;
    }

    return @"";
}

- (NSString *)cleanString
{
    return [self stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
}

- (NSString *)trimmedString
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)stringByRemovingCharactersInSet:(NSCharacterSet *)characterSet
{
    return [[self componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
}

- (NSString *)stringByRemovingNonDigitCharacters
{
    return [self stringByRemovingCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]];
}

- (NSString *)stringByRemovingFirstKeyPathComponent
{
    NSRange firstDotRange = [self rangeOfString:@"."];

    if (firstDotRange.length > 0)
    {
        return [self substringFromIndex:NSMaxRange(firstDotRange)];
    }
    else
    {
        return self;
    }
}

- (BOOL)startsWith:(NSString *)string
{
    return [self rangeOfString:string].location == 0;
}

- (BOOL)containsString:(NSString *)string
{
    return [self rangeOfString:string].location != NSNotFound;
}

- (NSString *)dateStringConvertedFromFormat:(NSString *)inputFormat
                                   toFormat:(NSString *)outputFormat
                          isSourceDateInUTC:(BOOL)isSourceDateInUTC
{
    static NSDateFormatter *formatter;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        formatter = [NSDateFormatter new];
    });

    formatter.dateFormat = inputFormat;
    formatter.timeZone = isSourceDateInUTC ? [NSTimeZone timeZoneForSecondsFromGMT:0] : [NSTimeZone localTimeZone];
    NSDate *date = [formatter dateFromString:self];

    formatter.dateFormat = outputFormat;
    formatter.timeZone = [NSTimeZone localTimeZone];
    return [formatter stringFromDate:date];
}

@end