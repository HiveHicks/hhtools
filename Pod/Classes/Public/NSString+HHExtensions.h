//
// Created by Andrey Yutkin on 05.02.14.
//

#import <Foundation/Foundation.h>


@interface NSString (HHExtensions)

+ (BOOL)isNilOrEmpty:(NSString *)string;

- (BOOL)isEmpty;
- (BOOL)isValidEmail;

- (NSString *)URLEncodedString;

- (NSString *)cleanString;
- (NSString *)trimmedString;

- (NSString *)stringByRemovingCharactersInSet:(NSCharacterSet *)characterSet;
- (NSString *)stringByRemovingNonDigitCharacters;
- (NSString *)stringByRemovingFirstKeyPathComponent;

- (BOOL)startsWith:(NSString *)string;
- (BOOL)containsString:(NSString *)string;

- (NSString *)dateStringConvertedFromFormat:(NSString *)inputFormat
                                   toFormat:(NSString *)outputFormat
                          isSourceDateInUTC:(BOOL)isSourceDateInUTC;

@end