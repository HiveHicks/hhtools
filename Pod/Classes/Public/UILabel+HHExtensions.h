//
// Created by HiveHicks on 14.03.14.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UILabel (HHExtensions)

+ (UILabel *)labelWithFont:(UIFont *)font color:(UIColor *)color text:(NSString *)text;
+ (UILabel *)labelWithFont:(UIFont *)font color:(UIColor *)color;

- (void)setText:(NSString *)text withAttributes:(NSDictionary *)attributes;

@end