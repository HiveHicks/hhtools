//
// Created by HiveHicks on 04.01.15.
//

#import <Foundation/Foundation.h>


@interface NSPredicate (HHExtensions)

+ (NSPredicate *)predicateWithValue:(id)value forKeyPath:(NSString *)keyPath;
+ (NSPredicate *)notNullPredicateForKorKeyPath:(NSString *)keyPath;

@end