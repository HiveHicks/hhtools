//
// Created by Andrey Yutkin on 16.04.14.
//

#import "NSDate+HHExtensions.h"


@implementation NSDate (HHExtensions)

- (BOOL)isLaterThanDate:(NSDate *)date
{
    return [self compare:date] == NSOrderedDescending;
}

- (BOOL)isLaterThanOrEqualToDate:(NSDate *)date
{
    return ![self isEarlierThanDate:date];
}

- (BOOL)isEarlierThanDate:(NSDate *)date
{
    return [self compare:date] == NSOrderedAscending;
}

- (BOOL)isEarlierThanOrEqualToDate:(NSDate *)date
{
    return ![self isLaterThanDate:date];
}

@end