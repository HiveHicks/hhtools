//
// Created by Andrey Yutkin on 14.11.13.
// Copyright (c) 2013 anywayanyday. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UIButton (Layout)

- (void)placeBaselineAtY:(CGFloat)y;
- (void)alignBaselineWithSiblingLabel:(UILabel *)label;

@end