//
// Created by HiveHicks on 25.05.14.
//

#import "NSMutableArray+HHExtensions.h"


@implementation NSMutableArray (HHExtensions)

- (BOOL)addNonNilObject:(id)object
{
    if (object)
    {
        [self addObject:object];
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)addOrReplaceObject:(id)object
{
    NSUInteger index = [self indexOfObject:object];

    if (index == NSNotFound) {
        [self addObject:object];
    } else {
        self[index] = object;
    }
}

@end