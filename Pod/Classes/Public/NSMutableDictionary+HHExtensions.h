//
// Created by HiveHicks on 18.01.15.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (HHExtensions)

/**
* Пытается извлечь из словаря значение по ключу, а в случае его отсутствия выполняет fallbackBlock и записывает
* возвращенное им значение под этим ключом. Метод возвращает либо существовавшее значение, либо возвращенное из
* fallbackBlock.
*/
- (id)objectForKey:(id)key fallbackBlock:(id (^)(id))fallbackBlock;

@end