//
// Created by HiveHicks on 09.01.15.
//

#import "UIViewController+HHExtensions.h"


@implementation UIViewController (HHExtensions)

- (UINavigationController *)inNavigationController
{
    if (self.navigationController) {
        return self.navigationController;
    } else {
        return [[UINavigationController alloc] initWithRootViewController:self];
    }
}

- (void)replaceChildViewController:(UIViewController *)oldChildVC
                withViewController:(UIViewController *)newChildVC
                   inContainerView:(UIView *)containerView
{
    if (oldChildVC) {
        [oldChildVC willMoveToParentViewController:nil];
    }

    NSDictionary *viewsDictionary = @{
            @"childView" : newChildVC.view,
            @"containerView" : containerView
    };

    [newChildVC.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addChildViewController:newChildVC];

    [containerView addSubview:newChildVC.view];

    [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[childView]|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:viewsDictionary]];

    [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[childView]|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:viewsDictionary]];

    [newChildVC didMoveToParentViewController:self];

    if (oldChildVC) {
        [oldChildVC removeFromParentViewController];
    }
}

@end