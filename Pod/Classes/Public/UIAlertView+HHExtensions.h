//
// Created by HiveHicks on 02.01.15.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UIAlertView (HHExtensions)

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;
+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString *)cancelButtonTitle;

@end