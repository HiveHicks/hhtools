//
//  HHMacros.h
//  HHTools
//
//  Created by HiveHicks on 01.01.15.
//
//

#import <UIKit/UIColor.h>
#import "NSString+HHExtensions.h"

#define VALUE_OR_NSNULL(x) ((x) ? (x) : [NSNull null])
#define VALUE_OR_NIL(x) ((x) == (id)[NSNull null] ? nil : (x))

#define KEY_PATH(property) [(@""#property) stringByRemovingFirstKeyPathComponent]

#define ASSIGN_IF_CHANGED(A, B) \
if (![A isEqual:B] || (A == nil && B != nil)) { \
A = B; \
}

#define IF_CAST(VAR1, CLASS, VAR2, CODE) \
if ([VAR1 isKindOfClass:[CLASS class]]) \
{ \
CLASS *VAR2 = (CLASS *)VAR1; \
CODE \
}

#define DOCUMENTS_DIR ((NSString *)NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject)
#define LIBRARY_DIR ((NSString *)NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES).firstObject)

#define UIColorFromHex(rgbValue) \
[UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:1.0]