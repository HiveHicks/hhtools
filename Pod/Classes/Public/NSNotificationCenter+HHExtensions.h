//
// Created by Andrey Yutkin on 21.02.14.
//

#import <Foundation/Foundation.h>


@interface NSNotificationCenter (HHExtensions)

- (void)postNotificationOnMainThreadWithName:(NSString *)name object:(id)object;

@end