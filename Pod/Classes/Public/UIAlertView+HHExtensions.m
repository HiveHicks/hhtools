//
// Created by HiveHicks on 02.01.15.
//

#import "UIAlertView+HHExtensions.h"


@implementation UIAlertView (HHExtensions)

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    return [UIAlertView showAlertWithTitle:title message:message cancelButtonTitle:NSLocalizedString(@"LocOK", nil)];
}

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString *)cancelButtonTitle
{
    UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle:title
                                       message:message
                                      delegate:nil
                             cancelButtonTitle:cancelButtonTitle
                             otherButtonTitles:nil];
    [alert show];
}

@end