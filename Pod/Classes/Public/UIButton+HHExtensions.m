//
// Created by HiveHicks on 25.02.14.
//

#import "UIButton+HHExtensions.h"


@implementation UIButton (HHExtensions)

+ (UIButton *)buttonWithImage:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button sizeToFit];
    return button;
}

+ (instancetype)buttonWithTitle:(NSString *)title
{
    UIButton *button = [[self alloc] init];
    [button setTitle:title forState:UIControlStateNormal];
    [button sizeToFit];
    return button;
}

- (void)centerImageAndTitleWithSpacing:(CGFloat)spacing
{
    CGSize imageSize = self.imageView.image.size;
    CGSize titleSize = [self.titleLabel sizeThatFits:CGSizeMake(MAXFLOAT, MAXFLOAT)];

    // lower the text and push it left so it appears centered below the image
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0f, -imageSize.width, -(imageSize.height + spacing), 0.0f);

    // raise the image and push it right so it appears centered above the text
    self.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0f, 0.0f, -titleSize.width);
}

@end