//
// Created by HiveHicks on 28.03.15.
// Copyright (c) 2015 HiveHicks. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSDateComponents (HHExtensions)

// TODO: поддерживаются не все юниты
- (NSInteger)hh_valueForComponent:(NSCalendarUnit)unit;
- (void)hh_setValue:(NSInteger)value forComponent:(NSCalendarUnit)unit;

@end