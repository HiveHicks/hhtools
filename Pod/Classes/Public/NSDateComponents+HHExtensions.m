//
// Created by HiveHicks on 28.03.15.
// Copyright (c) 2015 HiveHicks. All rights reserved.
//

#import "NSDateComponents+HHExtensions.h"


@implementation NSDateComponents (HHExtensions)

- (NSInteger)hh_valueForComponent:(NSCalendarUnit)unit
{
    switch (unit)
    {
        case NSCalendarUnitDay:
            return self.day;
        case NSCalendarUnitWeekOfMonth:
        case (NSCalendarUnit)kCFCalendarUnitWeek:
            return self.weekOfMonth;
        case NSCalendarUnitMonth:
            return self.month;
        case NSCalendarUnitYear:
            return self.year;
        default:
            NSLog(@"Warning! Unit %u not supported by %@", unit, NSStringFromSelector(_cmd));
            return 0;
    }
}

- (void)hh_setValue:(NSInteger)value forComponent:(NSCalendarUnit)unit
{
    if (unit & NSCalendarUnitDay) {
        self.day = value;
    }
    if (unit & NSCalendarUnitWeekOfMonth || unit & kCFCalendarUnitWeek) {
        self.weekOfMonth = value;
    }
    if (unit & NSCalendarUnitMonth) {
        self.month = value;
    }
    if (unit & NSCalendarUnitYear) {
        self.year = value;
    }
    if (unit & NSCalendarUnitHour) {
        self.hour = value;
    }
    if (unit & NSCalendarUnitMinute) {
        self.minute = value;
    }
    if (unit & NSCalendarUnitSecond) {
        self.second = value;
    }
    if (unit & NSCalendarUnitNanosecond) {
        self.nanosecond = value;
    }
}

@end
