//
// Created by hivehicks on 6/8/12.
//
//  ${FILENAME}
//  AutoMobile
//
//  Created by ${FULLUSERNAME} on 6/8/12.
//  Copyright (c) 2012 ${ORGANIZATIONNAME}. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "UIView+HHLayout.h"


@implementation UIView (HHLayout)

- (CGFloat)minX
{
    return self.frame.origin.x;
}

- (CGFloat)minY
{
    return self.frame.origin.y;
}

- (CGFloat)maxX
{
    return self.frame.origin.x + self.frame.size.width;
}

- (CGFloat)maxY
{
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setMinY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (void)placeAtPoint:(CGPoint)point corner:(HHViewCorner)corner relativeToSuperviewCorner:(HHViewCorner)superviewCorner
{
    CGRect frame = self.frame;

    //
    // Транслируем систему координат относительно superviewCorner в исходную
    //
    if (superviewCorner == kHHViewCornerTopRight || superviewCorner == kHHViewCornerBottomRight) {
        frame.origin.x = self.superview.bounds.size.width - point.x;
    } else {
        frame.origin.x = point.x;
    }

    if (superviewCorner == kHHViewCornerBottomLeft || superviewCorner == kHHViewCornerBottomRight) {
        frame.origin.y = self.superview.bounds.size.height - point.y;
    } else {
        frame.origin.y = point.y;
    }

    //
    // Помещаем нужный угол в нужную точку
    //
    if (corner == kHHViewCornerTopRight || corner == kHHViewCornerBottomRight) {
        frame.origin.x -= frame.size.width;
    }

    if (corner == kHHViewCornerBottomLeft || corner == kHHViewCornerBottomRight) {
        frame.origin.y -= frame.size.height;
    }

    [self setFrame:frame];
}

- (void)placeAtX:(CGFloat)xCoordinate
          corner:(HHViewCorner)corner
        relativeToSuperviewCorner:(HHViewCorner)superviewCorner
{
    [self placeAtPoint:CGPointMake(xCoordinate, self.frame.origin.y) corner:corner relativeToSuperviewCorner:superviewCorner];
}

- (void)placeAtY:(CGFloat)yCoordinate corner:(HHViewCorner)corner relativeToSuperviewCorner:(HHViewCorner)superviewCorner
{
    [self placeAtPoint:CGPointMake(self.frame.origin.x, yCoordinate) corner:corner relativeToSuperviewCorner:superviewCorner];
}

- (void)placeAtPoint:(CGPoint)point
{
    [self placeAtPoint:point corner:kHHViewCornerTopLeft relativeToSuperviewCorner:kHHViewCornerTopLeft];
}

- (void)placeAtX:(CGFloat)xCoordinate
{
    [self placeAtX:xCoordinate corner:kHHViewCornerTopLeft relativeToSuperviewCorner:kHHViewCornerTopLeft];
}

- (void)placeAtY:(CGFloat)yCoordinate
{
    [self placeAtY:yCoordinate corner:kHHViewCornerTopLeft relativeToSuperviewCorner:kHHViewCornerTopLeft];
}

- (void)placeCenterAtX:(CGFloat)x
{
    self.center = CGPointMake(x, self.center.y);
}

- (void)placeCenterAtY:(CGFloat)y
{
    self.center = CGPointMake(self.center.x, y);
}

- (void)attachToRightSuperviewEdge
{
    [self placeAtX:0 corner:kHHViewCornerTopRight relativeToSuperviewCorner:kHHViewCornerTopRight];
}

- (void)attachToBottomSuperviewEdge
{
    [self placeAtY:0 corner:kHHViewCornerBottomLeft relativeToSuperviewCorner:kHHViewCornerBottomLeft];

    self.autoresizingMask =
            (self.autoresizingMask | UIViewAutoresizingFlexibleTopMargin) & (~UIViewAutoresizingFlexibleBottomMargin);
}


- (void)moveByX:(CGFloat)xDiff
{
    [self placeAtX:(self.frame.origin.x + xDiff)];
}

- (void)moveByY:(CGFloat)yDiff
{
    [self placeAtY:(self.frame.origin.y + yDiff)];
}

- (void)centerInSuperview
{
//    [self placeAtPoint:CGPointMake((self.superview.bounds.size.width - self.frame.size.width) / 2,
//                                   (self.superview.bounds.size.height - self.frame.size.height) / 2)];

    [self centerHorizontallyInSuperview];
    [self centerVerticallyInSuperview];
}

- (void)centerInSuperviewRect:(CGRect)rect
{
    CGFloat leftMargin = (rect.size.width - self.width) / 2;
    CGFloat topMargin = (rect.size.height - self.height) / 2;
    [self placeAtPoint:CGPointMake(rect.origin.x + leftMargin, rect.origin.y + topMargin)];
}

- (void)centerHorizontallyInSuperview
{
    [self placeAtX:floorf((self.superview.bounds.size.width - self.frame.size.width) / 2)];

    self.autoresizingMask |= UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
}

- (void)centerHorizontallyInSuperviewAfterSettingWidth:(CGFloat)width
{
    self.frame = CGRectMake((self.superview.width - width) / 2, self.minY, width, self.height);
}

- (void)centerVerticallyInSuperview
{
    [self placeAtY:floorf((self.superview.bounds.size.height - self.frame.size.height) / 2)];

    self.autoresizingMask |= UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
}

- (void)centerVerticallyInSuperviewRect:(CGRect)rect
{
    CGFloat topMargin = (rect.size.height - self.height) / 2;
    [self placeAtY:(rect.origin.y + topMargin)];
}

- (void)alignCentersHorizontallyWithView:(UIView *)view
{
    CGFloat centerX = [self.superview convertPoint:view.center fromView:view.superview].x;
    self.center = CGPointMake(centerX, self.center.y);
}

- (void)alignCentersVerticallyWithView:(UIView *)view
{
    CGFloat centerY = [self.superview convertPoint:view.center fromView:view.superview].y;
    self.center = CGPointMake(self.center.x, centerY);
}

- (CGFloat)width
{
    return self.bounds.size.width;
}

- (CGFloat)height
{
    return self.bounds.size.height;
}

- (void)setWidth:(CGFloat)width
{
    [self setWidth:width preservingCenter:NO];
}

- (void)setWidth:(CGFloat)width preservingCenter:(BOOL)preserveCenter
{
    CGFloat originalCenterX = self.center.x;

    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;

    if (preserveCenter) {
        self.center = CGPointMake(originalCenterX, self.center.y);
    }
}

- (void)setHeight:(CGFloat)height
{
    [self setHeight:height preservingCenter:NO];
}

- (void)setHeight:(CGFloat)height preservingCenter:(BOOL)preserveCenter
{
    CGFloat originalCenterY = self.center.y;

    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;

    if (preserveCenter) {
        self.center = CGPointMake(self.center.x, originalCenterY);
    }
}

- (void)setFrameSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (void)setSize:(CGSize)size
{
    [self setFrameSize:size];
}

- (void)sizeToFitWidth:(CGFloat)width
{
    [self setFrameSize:[self sizeThatFits:CGSizeMake(width, MAXFLOAT)]];
}

@end