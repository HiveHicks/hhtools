//
//  UILabel+HHLayout.h
//  AutoMobile
//
//  Created by HiveHicks on 02.04.12.
//  Copyright (c) 2012 Seedway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UILabel (HHLayout)

- (CGSize)sizeConstrainedToSuperviewBounds;
- (CGSize)sizeConstrainedToWidth:(CGFloat)width;

- (void)resizeHorizontallyToFitContent;
- (void)resizeHorizontallyConstrainedToWidth:(CGFloat)maxWidth;
- (void)resizeConstrainedToWidth:(CGFloat)maxWidth;

- (void)resizeVerticallyConstrainedToWidth:(CGFloat)maxWidth;
- (void)resizeVerticallyToFitContent;

- (CGFloat)lastBaseline;
- (void)placeFirstBaselineAtY:(CGFloat)y;
- (void)placeLastBaselineAtY:(CGFloat)y;
- (void)alignBaselineWithSiblingLabel:(UILabel *)label;

@end
