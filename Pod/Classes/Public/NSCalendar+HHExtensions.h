//
// Created by HiveHicks on 14.04.14.
//

#import <Foundation/Foundation.h>


@interface NSCalendar (HHExtensions)

- (NSDate *)dateByAddingHours:(NSInteger)hours toDate:(NSDate *)date;
- (NSDate *)dateByAddingDays:(NSInteger)days toDate:(NSDate *)date;
- (NSDate *)dateByAddingMonths:(NSInteger)months toDate:(NSDate *)date;
- (NSDate *)dateByAddingYears:(NSInteger)years toDate:(NSDate *)date;

@end