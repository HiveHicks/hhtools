//
// Created by HiveHicks on 02.01.15.
//

#import "HHUtils.h"


BOOL HHIsIOS7OrLater()
{
    return floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1;
}