//
// Created by Andrey Yutkin on 22.12.14.
//

#import "NSHTTPCookieStorage+HHExtensions.h"
#import "NSArray+HHExtensions.h"


@implementation NSHTTPCookieStorage (HHExtensions)

- (NSHTTPCookie *)cookieWithName:(NSString *)name
{
    return [self.cookies filteredArrayUsingBlock:^BOOL(NSHTTPCookie *cookie) {
        return [cookie.name isEqual:name];
    }].firstObject;
}

@end