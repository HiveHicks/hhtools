//
// Created by HiveHicks on 09.03.14.
//

#import "NSSet+HHExtensions.h"


@implementation NSSet (HHExtensions)

- (NSSet *)setByRemovingObjects:(NSSet *)set
{
    NSMutableSet *tempSet = [self mutableCopy];
    [tempSet minusSet:set];
    return tempSet;
}

@end