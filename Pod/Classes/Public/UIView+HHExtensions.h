//
// Created by Andrey Yutkin on 23.12.13.
// Copyright (c) 2013 anywayanyday. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


extern UIViewAnimationOptions UIViewAnimationOptionsFromCurve(UIViewAnimationCurve curve);


@interface UIView (HHExtensions)

- (UIView *)closestAncestorOfClass:(Class)class;

- (void)addSubviews:(NSArray *)views;

- (UIView *)firstResponder;
- (BOOL)findAndResignFirstResponder;

- (void)showFullyTransparent;

- (void)removeAllSubviews;

+ (NSPredicate *)visiblePredicate;

+ (UIView *)viewWithBackgroundColor:(UIColor *)color;

+ (void)hh_performWithoutAnimations:(void (^)())block;

@end