//
// Created by HiveHicks on 05.03.14.
//

#import <Foundation/Foundation.h>


@interface NSMutableAttributedString (HHExtensions)

- (void)appendString:(NSString *)string;
- (void)appendString:(NSString *)string withAttributes:(NSDictionary *)attributes;

@end