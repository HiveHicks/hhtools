//
// Created by HiveHicks on 18.01.15.
//

#import "NSMutableDictionary+HHExtensions.h"


@implementation NSMutableDictionary (HHExtensions)

- (id)objectForKey:(id)key fallbackBlock:(id (^)(id key))fallbackBlock
{
    id object = self[key];

    if (object == nil)
    {
        object = fallbackBlock(key);

        if (object) {
            self[key] = object;
        }
    }

    return object;
}

@end