//
//  DateRange.h
//  AutoMobile
//
//  Created by HiveHicks on 31.01.12.
//  Copyright (c) 2012 Seedway. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HHDateRange : NSObject <NSCopying>

@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;

@property (nonatomic, readonly) BOOL includesStartDate;
@property (nonatomic, readonly) BOOL includesEndDate;

- (instancetype)initWithStartDate:(NSDate *)startDate
                        inclusive:(BOOL)includesStartDate
                          endDate:(NSDate *)endDate
                        inclusive:(BOOL)includesEndDate;

+ (instancetype)rangeFromDate:(NSDate *)startDate
                    inclusive:(BOOL)includesStartDate
                       toDate:(NSDate *)endDate
                    inclusive:(BOOL)includesEndDate;

- (BOOL)includesDate:(NSDate *)date;

/**
* Возвращает массив HHDateRange, описывающих месячные периоды, попадающие в текущий range.
*
* Например, если текущий период: [13.02.2014; 9.04.2014), то будет возвращен такой массив:
* 0. [13.02.2014; 1.03.2014)
* 1. [1.03.2014; 1.04.2014)
* 2. [1.04.2014; 9.04.2014)
*/
- (NSArray *)monthRanges;

/**
* По аналогии с monthRanges — возвращает массив HHDateRange, описывающих годовые периоды, попадающие в текущий range.
*/
- (NSArray *)yearRanges;

/**
 * По аналогии с monthRanges — возвращает массив HHDateRange, описывающих дневные периоды, попадающие в текущий range.
 */
- (NSArray *)dayRanges;

- (NSTimeInterval)timeInterval;

@end
