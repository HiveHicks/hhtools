//
// Created by Andrey Yutkin on 16.04.14.
//

#import <Foundation/Foundation.h>


@interface NSDate (HHExtensions)

- (BOOL)isLaterThanDate:(NSDate *)date;
- (BOOL)isLaterThanOrEqualToDate:(NSDate *)date;
- (BOOL)isEarlierThanDate:(NSDate *)date;
- (BOOL)isEarlierThanOrEqualToDate:(NSDate *)date;

@end