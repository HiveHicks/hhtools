//
// Created by Andrey Yutkin on 01.04.14.
// Copyright (c) 2014 Andrey Yutkin. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "HHButton.h"


@implementation HHButton {
    NSMutableDictionary *_backgroundColors;
    NSMutableDictionary *_borderColors;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _init];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self _init];
    }
    return self;
}

- (void)_init
{
    _backgroundColors = [NSMutableDictionary new];
    _borderColors = [NSMutableDictionary new];
}

- (void)setBackgroundColor:(UIColor *)color forState:(UIControlState)state
{
    [self setColor:color forState:state inDictionary:_backgroundColors];
    [self updateColors];
}

- (UIColor *)backgroundColorForState:(UIControlState)state
{
    UIColor *color = [self colorForState:state inDictionary:_backgroundColors];

    if (color) {
        return color;
    } else {
        return self.backgroundColor;
    }
}

- (void)setBorderColor:(UIColor *)color forState:(UIControlState)state
{
    [self setColor:color forState:state inDictionary:_borderColors];
    [self updateColors];
}

- (UIColor *)borderColorForState:(UIControlState)state
{
    UIColor *color = [self colorForState:state inDictionary:_borderColors];

    if (color) {
        return color;
    } else {
        return nil;
    }
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self updateColors];
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self updateColors];
}

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    [self updateColors];
}

- (void)setColor:(UIColor *)color forState:(UIControlState)state inDictionary:(NSMutableDictionary *)dict
{
    if (color != nil) {
        dict[@(state)] = color;
    }
}

- (UIColor *)colorForState:(UIControlState)state inDictionary:(NSMutableDictionary *)dict
{
    UIColor *color = dict[@(state)];
    if (color == nil) {
        color = dict[@(UIControlStateNormal)];
    }
    return color;
}

- (void)updateColors
{
    self.backgroundColor = [self backgroundColorForState:self.state];
    self.layer.borderColor = [self borderColorForState:self.state].CGColor;
}

@end