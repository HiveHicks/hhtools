//
// Created by HiveHicks on 01.01.15.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UIScrollView (HHKeyboardNotifications)

- (void)registerForKeyboardNotifications;
- (void)registerForKeyboardNotificationsInView:(UIView *)topView;

- (void)handleKeyboardNotification:(NSNotification *)notification inView:(UIView *)topView;
- (void)handleKeyboardNotification:(NSNotification *)notification
                            inView:(UIView *)topView
                        animations:(void (^)())animations;

@end