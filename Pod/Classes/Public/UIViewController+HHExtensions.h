//
// Created by HiveHicks on 09.01.15.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UIViewController (HHExtensions)

- (UINavigationController *)inNavigationController;

- (void)replaceChildViewController:(UIViewController *)oldChildVC
                withViewController:(UIViewController *)newChildVC
                   inContainerView:(UIView *)containerView;

@end