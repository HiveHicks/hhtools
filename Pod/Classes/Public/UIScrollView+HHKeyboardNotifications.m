//
// Created by HiveHicks on 01.01.15.
//

#import "UIScrollView+HHKeyboardNotifications.h"
#import "UIView+HHExtensions.h"
#import "objc/runtime.h"

static char kWillShowKey, kWillHideKey;


@implementation UIScrollView (HHKeyboardNotifications)

- (void)registerForKeyboardNotificationsInView:(UIView *)topView
{
    __weak UIView *weakTopView = topView;
    __weak UIScrollView *weakScrollView = self;

    void (^block)(NSNotification *) = ^(NSNotification *notification) {

        UIView *strongTopView = weakTopView;
        UIScrollView *strongScrollView = weakScrollView;

        if (strongScrollView && strongTopView) {
            [strongScrollView handleKeyboardNotification:notification inView:strongTopView];
        }
    };

    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserverForName:UIKeyboardWillShowNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:block];
    [nc addObserverForName:UIKeyboardWillHideNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:block];
}

- (void)registerForKeyboardNotifications
{
    [self registerForKeyboardNotificationsInView:self];
}

- (void)handleKeyboardNotification:(NSNotification *)notification inView:(UIView *)topView
{
    [self handleKeyboardNotification:notification inView:topView animations:nil];
}

- (void)handleKeyboardNotification:(NSNotification *)notification
                            inView:(UIView *)topView
                        animations:(void (^)())animations
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];

    NSDictionary *userInfo = notification.userInfo;

    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationCurve =
            (UIViewAnimationCurve) [userInfo[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];

    CGRect keyboardEndRect = [topView convertRect:[userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue]
                                         fromView:window];
    CGFloat keyboardHeight = keyboardEndRect.size.height;

    BOOL appearing = [notification.name isEqual:UIKeyboardWillShowNotification];

    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:UIViewAnimationOptionsFromCurve(animationCurve)
                     animations:^{

                         UIEdgeInsets insets = self.contentInset;
                         insets.bottom = appearing ? keyboardHeight : 0.0f;

                         self.contentInset = insets;
                         self.scrollIndicatorInsets = insets;

                         if (animations) {
                             animations();
                         }
                     }
                     completion:nil];
}

@end