//
// Created by HiveHicks on 28.03.14.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSParagraphStyle (HHExtensions)

+ (NSParagraphStyle *)paragraphStyleWithLineHeight:(CGFloat)lineHeight;
+ (NSParagraphStyle *)paragraphStyleWithLineHeight:(CGFloat)lineHeight alignment:(NSTextAlignment)alignment;
+ (NSParagraphStyle *)paragraphStyleWithLineHeight:(CGFloat)lineHeight paragraphSpacing:(CGFloat)paragraphSpacing;

@end