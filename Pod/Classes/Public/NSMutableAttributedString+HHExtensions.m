//
// Created by HiveHicks on 05.03.14.
//

#import "NSMutableAttributedString+HHExtensions.h"


@implementation NSMutableAttributedString (HHExtensions)

- (void)appendString:(NSString *)string
{
    [self appendAttributedString:[[NSAttributedString alloc] initWithString:string]];
}

- (void)appendString:(NSString *)string withAttributes:(NSDictionary *)attributes
{
    if (string) {
        [self appendAttributedString:[[NSAttributedString alloc] initWithString:string attributes:attributes]];
    }
}

@end