//
// Created by Andrey Yutkin on 22.12.14.
//

#import <Foundation/Foundation.h>


@interface NSHTTPCookieStorage (HHExtensions)

/// Возвращает первую куку с указанным именем
- (NSHTTPCookie *)cookieWithName:(NSString *)name;

@end