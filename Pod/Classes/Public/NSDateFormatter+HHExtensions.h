//
// Created by Andrey Yutkin on 07.08.14.
//

#import <Foundation/Foundation.h>


@interface NSDateFormatter (HHExtensions)

+ (NSDateFormatter *)formatterWithFormat:(NSString *)format;
+ (NSDateFormatter *)sharedFormatterWithFormat:(NSString *)format;

@end