//
//  DateRange.m
//  AutoMobile
//
//  Created by HiveHicks on 31.01.12.
//  Copyright (c) 2012 Seedway. All rights reserved.
//

#import "HHDateRange.h"
#import "NSDateComponents+HHExtensions.h"


@implementation HHDateRange

+ (instancetype)rangeFromDate:(NSDate *)startDate
                    inclusive:(BOOL)includesStartDate
                       toDate:(NSDate *)endDate
                    inclusive:(BOOL)includesEndDate
{
    return [[self alloc] initWithStartDate:startDate
                                 inclusive:includesStartDate
                                   endDate:endDate
                                 inclusive:includesEndDate];
}

- (instancetype)initWithStartDate:(NSDate *)startDate
                        inclusive:(BOOL)includesStartDate
                          endDate:(NSDate *)endDate
                        inclusive:(BOOL)includesEndDate
{
    NSComparisonResult comparison = [endDate compare:startDate];

    // If endDate < startDate, or endDate == startDate but either startDate or endDate is excluded from the range,
    // then the range is considered invalid.
    if (comparison == NSOrderedAscending || (comparison == NSOrderedSame && (!includesStartDate || !includesEndDate))) {
        return nil;
    }
    
    self = [super init];

    if (self)
    {
        _startDate = startDate;
        _endDate = endDate;
        _includesStartDate = includesStartDate;
        _includesEndDate = includesEndDate;
    }

    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    HHDateRange *copy = (HHDateRange *) [[[self class] allocWithZone:zone] init];

    if (copy != nil)
    {
        copy.startDate = self.startDate;
        copy.endDate = self.endDate;
        copy->_includesStartDate = _includesStartDate;
        copy->_includesEndDate = _includesEndDate;
    }

    return copy;
}

- (NSString *)debugDescription
{
    return [NSString stringWithFormat:@"%@ %@; %@ %@",
                                      _includesStartDate ? @"[" : @"(",
                                      _startDate,
                                      _endDate,
                                      _includesEndDate ? @"]" : @")"];
}

- (BOOL)includesDate:(NSDate *)date
{
    NSComparisonResult startResult = [date compare:_startDate];
    NSComparisonResult endResult = [date compare:_endDate];

    return (startResult == NSOrderedDescending || (_includesStartDate ? startResult == NSOrderedSame : NO)) &&
           (endResult == NSOrderedAscending || (_includesEndDate ? endResult == NSOrderedSame : NO));
}

- (NSArray *)monthRanges
{
    return [self _rangesForUnit:NSCalendarUnitMonth];
}

- (NSArray *)yearRanges
{
    return [self _rangesForUnit:NSCalendarUnitYear];
}

- (NSArray *)dayRanges
{
    return [self _rangesForUnit:NSCalendarUnitDay];
}

- (NSArray *)_rangesForUnit:(NSCalendarUnit)unit
{
    NSCalendar *calendar = [NSCalendar currentCalendar];

    NSMutableArray *ranges = [NSMutableArray new];

    NSDate *date = _startDate;
    BOOL included = _includesStartDate;

    while (YES)
    {
        NSDateComponents *components =
                [calendar components:(NSCalendarUnitDay | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:date];

        if (unit == NSCalendarUnitMonth || unit == NSCalendarUnitYear) {
            components.day = 1;
        }
        if (unit == NSCalendarUnitYear) {
            components.month = 1;
        }

        NSDate *unitStartDate = [calendar dateFromComponents:components];

        NSDateComponents *addedComponents = [NSDateComponents new];
        [addedComponents hh_setValue:1 forComponent:unit];

        NSDate *nextUnitStartDate = [calendar dateByAddingComponents:addedComponents toDate:unitStartDate options:0];

        if ([self includesDate:nextUnitStartDate])
        {
            [ranges addObject:[HHDateRange rangeFromDate:date inclusive:included
                                                  toDate:nextUnitStartDate inclusive:NO]];

            date = nextUnitStartDate;
            included = YES;
        }
        else
        {
            [ranges addObject:[HHDateRange rangeFromDate:date inclusive:included
                                                  toDate:_endDate inclusive:_includesEndDate]];

            break;
        }
    }

    return [ranges copy];
}

- (NSTimeInterval)timeInterval
{
    return [_endDate timeIntervalSinceDate:_startDate];
}

@end
