//
// Created by HiveHicks on 28.03.14.
//

#import "NSParagraphStyle+HHExtensions.h"


@implementation NSParagraphStyle (HHExtensions)

+ (NSParagraphStyle *)paragraphStyleWithLineHeight:(CGFloat)lineHeight
{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.minimumLineHeight = lineHeight;
    return paragraphStyle;
}

+ (NSParagraphStyle *)paragraphStyleWithLineHeight:(CGFloat)lineHeight alignment:(NSTextAlignment)alignment
{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.minimumLineHeight = lineHeight;
    paragraphStyle.alignment = alignment;
    return paragraphStyle;
}

+ (NSParagraphStyle *)paragraphStyleWithLineHeight:(CGFloat)lineHeight paragraphSpacing:(CGFloat)paragraphSpacing
{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.minimumLineHeight = lineHeight;
    paragraphStyle.paragraphSpacing = paragraphSpacing;
    return paragraphStyle;
}

@end