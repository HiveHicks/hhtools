//
//  NSArray+HiveHicksExtensions.h
//  AutoMobile
//
//  Created by HiveHicks on 31.01.12.
//  Copyright (c) 2012 Seedway. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSArray (HHExtensions)

- (id)firstObject;
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;

- (NSUInteger)lastIndexOfObject:(id)object;

- (NSArray *)flattenedArray;

- (NSArray *)arrayByRemovingObjectsInArray:(NSArray *)array;

- (NSArray *)arrayByPerformingSelector:(SEL)selector;
- (NSArray *)arrayByMappingObjectsUsingBlock:(id (^)(id object, NSUInteger index))block;
- (NSArray *)arrayByMappingObjectsUsingBlock:(id (^)(id object, NSUInteger index))block safe:(BOOL)safe;
- (NSArray *)map:(id (^)(id object))block;
- (NSArray *)safeMap:(id (^)(id object))block;

- (NSArray *)localizedStringsArray;

- (NSArray *)sortedArrayUsingArray:(NSArray *)array;

- (NSArray *)filteredArrayUsingBlock:(BOOL (^)(id object))block;
- (NSArray *)filteredArrayUsingPredicateWithFormat:(NSString *)format, ...;
- (id)firstObjectSatisfyingCondition:(BOOL (^)(id object))conditionBlock;

- (NSDictionary *)dictionaryByGroupingItemsByKey:(NSString *)key;

- (NSString *)SQLJoinedString;

@end
