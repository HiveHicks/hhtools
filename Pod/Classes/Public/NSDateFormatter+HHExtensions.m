//
// Created by Andrey Yutkin on 07.08.14.
//

#import "NSDateFormatter+HHExtensions.h"


@implementation NSDateFormatter (HHExtensions)

+ (NSDateFormatter *)formatterWithFormat:(NSString *)format
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = format;
    return formatter;
}

+ (NSDateFormatter *)sharedFormatterWithFormat:(NSString *)format
{
    static NSMutableDictionary *formatters;
    static dispatch_once_t once;

    dispatch_once(&once, ^{
        formatters = [NSMutableDictionary new];
    });

    NSDateFormatter *formatter = formatters[format];
    if (formatter == nil) {
        formatter = [NSDateFormatter formatterWithFormat:format];
        formatters[format] = formatter;
    }

    return formatter;
}

@end