//
// Created by HiveHicks on 25.02.14.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UIButton (HHExtensions)

+ (UIButton *)buttonWithImage:(UIImage *)image;
+ (instancetype)buttonWithTitle:(NSString *)title;

- (void)centerImageAndTitleWithSpacing:(CGFloat)spacing;

@end