//
// Created by HiveHicks on 04.01.15.
//

#import "NSPredicate+HHExtensions.h"


@implementation NSPredicate (HHExtensions)

+ (NSPredicate *)predicateWithValue:(id)value forKeyPath:(NSString *)keyPath
{
    return [NSPredicate predicateWithFormat:@"%K = %@", keyPath, value];
}

+ (NSPredicate *)notNullPredicateForKorKeyPath:(NSString *)keyPath
{
    return [NSPredicate predicateWithFormat:@"%K != NULL", keyPath];
}

@end