//
// Created by Andrey Yutkin on 23.12.13.
// Copyright (c) 2013 anywayanyday. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIView+HHExtensions.h"

UIViewAnimationOptions UIViewAnimationOptionsFromCurve(UIViewAnimationCurve curve)
{
    switch (curve)
    {
        case UIViewAnimationCurveEaseInOut:
            return UIViewAnimationOptionCurveEaseInOut;
        case UIViewAnimationCurveEaseIn:
            return UIViewAnimationOptionCurveEaseIn;
        case UIViewAnimationCurveEaseOut:
            return UIViewAnimationOptionCurveEaseOut;
        case UIViewAnimationCurveLinear:
        default:
            return UIViewAnimationOptionCurveLinear;
    }
}


@implementation UIView (HHExtensions)

- (UIView *)closestAncestorOfClass:(Class)class
{
    UIView *view = self;

    while (view.superview)
    {
        view = view.superview;

        if ([view isKindOfClass:class]) {
            return view;
        }
    }

    return nil;
}

- (void)addSubviews:(NSArray *)views
{
    for (UIView *view in views) {
        [self addSubview:view];
    }
}

- (UIView *)firstResponder
{
    if (self.isFirstResponder) {
        return self;
    }

    for (UIView *subview in self.subviews)
    {
        UIView *subviewFirstResponder = [subview firstResponder];
        if (subviewFirstResponder) {
            return subviewFirstResponder;
        }
    }

    return nil;
}

- (BOOL)findAndResignFirstResponder
{
    return [[self firstResponder] resignFirstResponder];
}

- (void)showFullyTransparent
{
    self.hidden = NO;
    self.alpha = 0.0f;
}

- (void)removeAllSubviews
{
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
}

+ (NSPredicate *)visiblePredicate
{
    return [NSPredicate predicateWithFormat:@"hidden = NO"];
}

+ (UIView *)viewWithBackgroundColor:(UIColor *)color
{
    UIView *view = [UIView new];
    view.backgroundColor = color;
    return view;
}

+ (void)hh_performWithoutAnimations:(void (^)())block;
{
    BOOL wasEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    block();
    [UIView setAnimationsEnabled:wasEnabled];
}

@end