//
// Created by HiveHicks on 11.03.14.
//

#import "NSDictionary+HHExtensions.h"
#import "NSString+HHExtensions.h"
#import "HHMacros.h"


@implementation NSDictionary (HHExtensions)

- (id)firstKeyForValue:(id)value
{
    for (id key in self)
    {
        id aValue = self[key];

        if ([aValue isEqual:value]) {
            return key;
        }
    }

    return nil;
}

- (NSArray *)arrayByMappingKeysAndValuesUsingBlock:(id (^)(id key, id value))block
{
    NSMutableArray *array = [NSMutableArray new];

    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [array addObject:block(key, obj)];
    }];

    return array;
}

- (NSString *)queryString
{
    NSMutableArray *pairs = [NSMutableArray array];

    for (NSString *key in [self keyEnumerator])
    {
        NSString *value = [VALUE_OR_NIL(self[key]) description];
        NSString *escapedValue = [value URLEncodedString] ?: @"";
        [pairs addObject:[NSString stringWithFormat:@"%@=%@", [[key description] URLEncodedString], escapedValue]];
    }

    return [pairs componentsJoinedByString:@"&"];
}

- (NSDictionary *)dictionaryByAddingEntriesFromDictionary:(NSDictionary *)dictionary
{
    NSMutableDictionary *finalDictionary = [NSMutableDictionary dictionaryWithDictionary:self];
    [finalDictionary addEntriesFromDictionary:dictionary];
    return finalDictionary;
}

- (NSDictionary *)dictionaryByRemovingNSNullValues
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:self.count];

    for (id key in self)
    {
        id value = self[key];

        if (value != [NSNull null]) {
            dict[key] = value;
        }
    }

    return dict;
}

@end