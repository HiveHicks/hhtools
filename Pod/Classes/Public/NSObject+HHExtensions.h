//
// Created by HiveHicks on 19.04.14.
//

#import <Foundation/Foundation.h>


@interface NSObject (HHExtensions)

+ (instancetype)dummy;

- (BOOL)isOneOf:(NSArray *)array;
- (BOOL)isNot:(id)object;

- (void)safeRemoveObserver:(id)observer forKeyPath:(NSString *)keyPath;

@end