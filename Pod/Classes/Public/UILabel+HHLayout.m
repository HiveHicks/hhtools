//
//  UILabel+HHLayout.m
//  AutoMobile
//
//  Created by HiveHicks on 02.04.12.
//  Copyright (c) 2012 Seedway. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "UILabel+HHLayout.h"
#import "UIView+HHLayout.h"

@implementation UILabel (HHLayout)

- (CGSize)sizeConstrainedToSuperviewBounds
{
    return [self sizeThatFits:self.superview.bounds.size];
}

- (CGSize)sizeConstrainedToWidth:(CGFloat)width
{
    CGSize size = [self sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    return CGSizeMake(MIN(width, size.width), size.height);
}

- (void)resizeHorizontallyToFitContent
{
    [self resizeHorizontallyConstrainedToWidth:self.superview.bounds.size.width];
}

- (void)resizeHorizontallyConstrainedToWidth:(CGFloat)maxWidth
{
    CGRect frame = self.frame;
    frame.size.width = [self sizeConstrainedToWidth:maxWidth].width;
    self.frame = frame;
}

- (void)resizeConstrainedToWidth:(CGFloat)maxWidth
{
    CGSize size = [self sizeThatFits:CGSizeMake(maxWidth, MAXFLOAT)];
    [self setFrameSize:CGSizeMake(MIN(maxWidth, size.width), size.height)];
}

- (void)resizeVerticallyConstrainedToWidth:(CGFloat)maxWidth
{
    CGFloat optimalHeight = [self sizeConstrainedToWidth:maxWidth].height;
    CGRect frame = self.frame;
    frame.size.height = optimalHeight;
    [self setFrame:frame];
}

- (void)resizeVerticallyToFitContent
{
    [self resizeVerticallyConstrainedToWidth:self.frame.size.width];
}

- (CGFloat)lastBaseline
{
    return self.maxY + self.font.descender;
}

- (void)placeFirstBaselineAtY:(CGFloat)y
{
    [self placeAtY:floorf(y - self.font.ascender)];
}

- (void)placeLastBaselineAtY:(CGFloat)y
{
    CGFloat selfBaseline = CGRectGetHeight(self.frame) + self.font.descender;
    CGFloat scale = [UIScreen mainScreen].scale;

    [self placeAtY:(floorf((y - selfBaseline) * scale) / scale)];
}

- (void)alignBaselineWithSiblingLabel:(UILabel *)label
{
    CGFloat targetBaseline = CGRectGetMaxY(label.frame) + label.font.descender;
    [self placeLastBaselineAtY:targetBaseline];
}


@end
