//
// Created by Andrey Yutkin on 14.11.13.
// Copyright (c) 2013 anywayanyday. All rights reserved.
//

#import "UIButton+Layout.h"
#import "UILabel+HHLayout.h"
#import "UIView+HHLayout.h"


@implementation UIButton (Layout)

- (void)placeBaselineAtY:(CGFloat)y
{
    [self placeAtY:(y - self.titleLabel.font.ascender - self.titleLabel.frame.origin.y)];
}

- (void)alignBaselineWithSiblingLabel:(UILabel *)label
{
    [self placeBaselineAtY:(label.frame.origin.y + label.font.ascender)];
}

@end