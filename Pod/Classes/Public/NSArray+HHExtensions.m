//
//  NSArray+HiveHicksExtensions.m
//  AutoMobile
//
//  Created by HiveHicks on 31.01.12.
//  Copyright (c) 2012 Seedway. All rights reserved.
//

#import <UIKit/UITableView.h>
#import "NSArray+HHExtensions.h"


@implementation NSArray (HHExtensions)

- (id)firstObject
{
    if ([self count] > 0)
        return [self objectAtIndex:0];
    return nil;
}

- (id)objectAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
}

- (NSUInteger)lastIndexOfObject:(id)object
{
    return [self indexOfObjectWithOptions:NSEnumerationReverse passingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return obj == object;
    }];
}

- (NSArray *)flattenedArray
{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:self.count];

    for (int i = 0; i < self.count; i++) {

        id object = [self objectAtIndex:i];

        if ([object isKindOfClass:[NSArray class]]) {
            [array addObjectsFromArray:[object flattenedArray]];
        } else {
            [array addObject:object];
        }
    }

    return [NSArray arrayWithArray:array];
}

- (NSArray *)arrayByRemovingObjectsInArray:(NSArray *)array
{
    NSMutableArray *result = [NSMutableArray arrayWithArray:self];
    [result removeObjectsInArray:array];
    return result;
}

- (NSArray *)arrayByPerformingSelector:(SEL)selector
{
    NSMutableArray *results = [NSMutableArray array];
    for (id item in self) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [results addObject:[item performSelector:selector]];
#pragma clang diagnostic pop
    }
    return results;
}

- (NSArray *)arrayByMappingObjectsUsingBlock:(id (^)(id object, NSUInteger index))block safe:(BOOL)safe
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count]];
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id object = block(obj, idx);
        if (object || !safe) {
            [result addObject:object];
        }
    }];
    return [NSArray arrayWithArray:result];
}

- (NSArray *)arrayByMappingObjectsUsingBlock:(id (^)(id object, NSUInteger index))block
{
    return [self arrayByMappingObjectsUsingBlock:block safe:NO];
}

- (NSArray *)map:(id (^)(id object))block
{
    return [self arrayByMappingObjectsUsingBlock:^(id object, NSUInteger index) {
        return block(object);
    }];
}

- (NSArray *)safeMap:(id (^)(id object))block
{
    return [self arrayByMappingObjectsUsingBlock:^(id object, NSUInteger index) {
        return block(object);
    } safe:YES];
}

- (NSArray *)localizedStringsArray
{
    NSMutableArray *localizedStrings = [NSMutableArray new];
    for (NSString *string in self) {
        [localizedStrings addObject:NSLocalizedString(string, nil)];
    }
    return [NSArray arrayWithArray:localizedStrings];
}

- (NSArray *)sortedArrayUsingArray:(NSArray *)otherArray
{
    return [self sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2)
            {
                NSUInteger index1 = [otherArray indexOfObject:obj1];
                NSUInteger index2 = [otherArray indexOfObject:obj2];
                
                if (index1 < index2) {
                    return NSOrderedAscending;
                } else if (index1 > index2) {
                    return NSOrderedDescending;
                } else {
                    return [obj1 compare:obj2];
                }
            }];
}

- (NSArray *)filteredArrayUsingBlock:(BOOL (^)(id))block
{
    return [self filteredArrayUsingPredicate:
            [NSPredicate predicateWithBlock:^BOOL(id object, NSDictionary *bindings) {
                return block(object);
            }]];
}

- (NSArray *)filteredArrayUsingPredicateWithFormat:(NSString *)format, ...
{
    va_list args;
    va_start(args, format);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:format arguments:args];
    va_end(args);

    return [self filteredArrayUsingPredicate:predicate];
}

- (id)firstObjectSatisfyingCondition:(BOOL (^)(id object))conditionBlock
{
    return [[self filteredArrayUsingBlock:conditionBlock] firstObject];
}

- (NSDictionary *)dictionaryByGroupingItemsByKey:(NSString *)key
{
    NSMutableDictionary *dict = [NSMutableDictionary new];

    for (id item in self)
    {
        id value = [item valueForKey:key];

        NSMutableArray *array = [dict objectForKey:value];
        if (array == nil) {
            array = [NSMutableArray new];
            [dict setObject:array forKey:value];
        }

        [array addObject:item];
    }

    return dict;
}

- (NSString *)SQLJoinedString
{
    return [[self map:^(id object) {
        return [NSString stringWithFormat:@"'%@'", object];
    }] componentsJoinedByString:@", "];
}

@end
