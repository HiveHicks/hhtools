//
// Created by hivehicks on 6/8/12.
//
//  ${FILENAME}
//  AutoMobile
//
//  Created by ${FULLUSERNAME} on 6/8/12.
//  Copyright (c) 2012 ${ORGANIZATIONNAME}. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, HHViewCorner) {
    kHHViewCornerTopLeft,
    kHHViewCornerTopRight,
    kHHViewCornerBottomLeft,
    kHHViewCornerBottomRight
};

@interface UIView (HHLayout)

- (CGFloat)minX;
- (CGFloat)minY;
- (CGFloat)maxX;
- (CGFloat)maxY;

- (void)setMinY:(CGFloat)y;

- (void)placeAtPoint:(CGPoint)point corner:(HHViewCorner)corner relativeToSuperviewCorner:(HHViewCorner)superviewCorner;
- (void)placeAtX:(CGFloat)xCoordinate corner:(HHViewCorner)corner relativeToSuperviewCorner:(HHViewCorner)superviewCorner;
- (void)placeAtY:(CGFloat)yCoordinate corner:(HHViewCorner)corner relativeToSuperviewCorner:(HHViewCorner)superviewCorner;

- (void)placeAtPoint:(CGPoint)point;
- (void)placeAtX:(CGFloat)xCoordinate;
- (void)placeAtY:(CGFloat)yCoordinate;

- (void)placeCenterAtX:(CGFloat)x;
- (void)placeCenterAtY:(CGFloat)y;

- (void)attachToRightSuperviewEdge;
- (void)attachToBottomSuperviewEdge;

- (void)moveByX:(CGFloat)xDiff;
- (void)moveByY:(CGFloat)yDiff;

- (void)centerInSuperview;
- (void)centerInSuperviewRect:(CGRect)rect;
- (void)centerHorizontallyInSuperview;
- (void)centerHorizontallyInSuperviewAfterSettingWidth:(CGFloat)width;
- (void)centerVerticallyInSuperview;
- (void)centerVerticallyInSuperviewRect:(CGRect)rect;

- (void)alignCentersHorizontallyWithView:(UIView *)view;
- (void)alignCentersVerticallyWithView:(UIView *)view;

- (CGFloat)width;
- (CGFloat)height;
- (void)setWidth:(CGFloat)width;
- (void)setWidth:(CGFloat)width preservingCenter:(BOOL)preserveCenter;
- (void)setHeight:(CGFloat)height;
- (void)setHeight:(CGFloat)height preservingCenter:(BOOL)preserveCenter;

- (void)setFrameSize:(CGSize)size;
- (void)setSize:(CGSize)size;   // same as setFrameSize

- (void)sizeToFitWidth:(CGFloat)width;

@end
