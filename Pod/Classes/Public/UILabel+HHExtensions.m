//
// Created by HiveHicks on 14.03.14.
//

#import "UILabel+HHExtensions.h"


@implementation UILabel (HHExtensions)

+ (UILabel *)labelWithFont:(UIFont *)font color:(UIColor *)color text:(NSString *)text
{
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.font = font;
    label.textColor = color;
    label.text = text;

    [label sizeToFit];

    return label;
}

+ (UILabel *)labelWithFont:(UIFont *)font color:(UIColor *)color
{
    return [self labelWithFont:font color:color text:nil];
}

- (void)setText:(NSString *)text withAttributes:(NSDictionary *)attributes
{
    NSMutableDictionary *allAttributes = [NSMutableDictionary dictionaryWithDictionary:@{
            NSFontAttributeName : self.font,
            NSForegroundColorAttributeName : self.textColor
    }];

    for (id key in attributes) {
        allAttributes[key] = attributes[key];
    }

    self.attributedText = [[NSAttributedString alloc] initWithString:(text ? text : @"") attributes:allAttributes];
}

@end