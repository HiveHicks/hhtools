//
// Created by HiveHicks on 19.04.14.
//

#import "NSObject+HHExtensions.h"


@implementation NSObject (HHExtensions)

+ (instancetype)dummy
{
    return nil;
}

- (BOOL)isOneOf:(NSArray *)array
{
    return [array containsObject:self];
}

- (BOOL)isNot:(id)object
{
    return ![self isEqual:object];
}

- (void)safeRemoveObserver:(id)observer forKeyPath:(NSString *)keyPath
{
    @try {
        [self removeObserver:observer forKeyPath:keyPath];
    }
    @catch (NSException *ex) {
    }
}

@end