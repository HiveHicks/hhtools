//
//  HHAppDelegate.h
//  HHTools
//
//  Created by CocoaPods on 01/01/2015.
//  Copyright (c) 2014 HiveHicks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
