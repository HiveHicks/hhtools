//
//  main.m
//  HHTools
//
//  Created by HiveHicks on 01/01/2015.
//  Copyright (c) 2014 HiveHicks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HHAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HHAppDelegate class]));
    }
}
