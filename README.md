# HHTools

[![CI Status](http://img.shields.io/travis/HiveHicks/HHTools.svg?style=flat)](https://travis-ci.org/HiveHicks/HHTools)
[![Version](https://img.shields.io/cocoapods/v/HHTools.svg?style=flat)](http://cocoadocs.org/docsets/HHTools)
[![License](https://img.shields.io/cocoapods/l/HHTools.svg?style=flat)](http://cocoadocs.org/docsets/HHTools)
[![Platform](https://img.shields.io/cocoapods/p/HHTools.svg?style=flat)](http://cocoadocs.org/docsets/HHTools)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HHTools is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "HHTools"

## Author

HiveHicks, hivehicks@gmail.com

## License

HHTools is available under the MIT license. See the LICENSE file for more info.

